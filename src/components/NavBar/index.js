import React from "react";
import { Button, Container, Nav, Navbar } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

import api from "../../services/api";
import { logout } from "../../services/auth";


const NavBar = () => {
  const location = useNavigate();

  async function exit() {
    await api.get("/logout");
    logout();
    window.location.reload();
  }

  return (
    <Navbar bg="primary" variant="dark">
        <Container>
            <Navbar.Brand href="#">Deixa No Azul</Navbar.Brand>
            <Nav className="me-auto">
                <Nav.Link href="/licenciadoadd">Cadastrar licenciado</Nav.Link>
                <Nav.Link href="/estabelecimentoadd">Cadastrar estabelecimento</Nav.Link>
                <Nav.Link href="/transacaoadd">Cadastrar transação</Nav.Link>
                <Nav.Link href="/contadeluzadd">Transação conta de luz</Nav.Link>
                <Nav.Link href="/comissoes">Comissões</Nav.Link>
            </Nav>
            <Button onClick={exit} variant="danger">Sair</Button>
        </Container>
    </Navbar>
  );
};

export default NavBar;