import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import RoutesExport from "./routes";

function App() {
  return (
      <RoutesExport />
  );
}

export default App;
