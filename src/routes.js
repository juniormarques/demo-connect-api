import React from "react";
import { BrowserRouter, Route, Routes,  Navigate } from "react-router-dom";
import { isAuthenticated } from "./services/auth";

import SingIn from "./pages/SignIn";
import LicenciadoAdd from "./pages/LicenciadoAdd";
import EstabelecimentoAdd from "./pages/EstabelecimentoAdd";
import TransacaoAdd from "./pages/TransacaoAdd";
import TransacaoContaDeLuzAdd from "./pages/TransacaoContaDeLuzAdd";
import Comissoes from "./pages/Comissoes";

const PrivateRoute = ({ children }) => {
    const authed = isAuthenticated() // retorna se o usuario esta autenticado
    
    return authed ? children : <Navigate to="/"  />;
}

const RoutesExport = () => (
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<SingIn />} />
      <Route path="/licenciadoadd" element={<PrivateRoute><LicenciadoAdd /></PrivateRoute>} />
      <Route path="/estabelecimentoadd" element={<PrivateRoute><EstabelecimentoAdd /></PrivateRoute>} />
      <Route path="/transacaoadd" element={<PrivateRoute><TransacaoAdd /></PrivateRoute>} />
      <Route path="/contadeluzadd" element={<PrivateRoute><TransacaoContaDeLuzAdd /></PrivateRoute>} />
      <Route path="/comissoes" element={<PrivateRoute><Comissoes/></PrivateRoute>} />
    </Routes>
  </BrowserRouter>
);

export default RoutesExport;