import React, { useState } from "react";
import { Alert, Button, Col, Container, Form, Row, Spinner } from "react-bootstrap";
import NavBar from "../../components/NavBar";

import api from "../../services/api";

const EstabelecimentoAdd = () => {
  const [error, setError] = useState("");

  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");

  const [ruaPessoal, setRuaPessoal] = useState("");
  const [numeroPessoal, setNumeroPessoal] = useState("");
  const [bairroPessoal, setBairroPessoal] = useState("");
  const [complementoPessoal, setComplementoPessoal] = useState("");
  const [cidadePessoal, setCidadePessoal] = useState("");
  const [cepPessoal, setCepPessoal] = useState("");
  const [ufPessoal, setUfPessoal] = useState("");

  const [ruaEstabelecimeto, setRuaEstabelecimeto] = useState("");
  const [numeroEstabelecimento, setNumeroEstabelecimento] = useState("");
  const [bairroEstabelecimento, setBairroEstabelecimento] = useState("");
  const [complementoEstabelecimento, setComplementoEstabelecimento] = useState("");
  const [cidadeEstabelecimento, setCidadeEstabelecimento] = useState("");
  const [cepEstabelecimento, setCepEstabelecimento] = useState("");
  const [ufEstabelecimento, setUfEstabelecimento] = useState("");

  const [cargo, setCargo] = useState("");
  const [estadoCivil, setEstadoCivil] = useState("");
  const [rg, setRg] = useState("");
  const [cpf, setCpf] = useState("");
  const [dataNascimento, setDataNascimento] = useState("");
  const [nomeFantasia, setNomeFantasia] = useState("");
  const [razaoSocial, setRazaoSocial] = useState("");
  const [cnpj, setCnpj] = useState("");
  const [cnae, setCnae] = useState("");
  const [sessaoCnae, setsessaoCnae] = useState("");
  const [tipoEmpresa, setTipoEmpresa] = useState("");
  const [telefonePessoal, setTelefonePessoal] = useState("");
  const [descricaotelefonePessoal, setDescricaoTelefonePessoal] = useState("");
  const [telefoneEstabelecimento, setTelefoneEstabelecimento] = useState("");
  const [serialNumber, setSerialNumber] = useState("");
  const [descricaotelefoneEstabelecimento, setDescricaoTelefoneEstabelecimento] = useState("");

  const [nomeBanco, setNomeBanco] = useState("");
  const [agencia, setAgencia] = useState("");
  const [numeroConta, setNumeroConta] = useState("");
  const [nomeTitular, setNomeTitular] = useState("");
  const [tipoConta, setTipoConta] = useState("");
  const [cpfCnpj, setCpfCnpj] = useState("");
  const [numeroBanco, setNumeroBanco] = useState("");

  const [loading, setLoading] = useState(false);

  async function handleEstabelecimento() {
    if (!email || !nome) {
      setError("Preencha os campos obrigatórios para continuar.");
    } else {
      // START LOADING
      setLoading(true)
      try {
        // RETORNA O LICENCIADO LOGADO
        const licenciado = await api.get("/licenciado");

        console.log(licenciado.data.data[0].id);

        await api.post("/estabelecimento", 
        { 
          email: email, 
          nome: nome,
          rua_pessoal: ruaPessoal,
          numero_pessoal: numeroPessoal,
          bairro_pessoal: bairroPessoal,
          complemento_pessoal: complementoPessoal,
          cidade_pessoal: cidadePessoal,
          cep_pessoal: cepPessoal,
          uf_pessoal: ufPessoal,
          rua: ruaEstabelecimeto,
          numero: numeroEstabelecimento,
          bairro: bairroEstabelecimento,
          complemento: complementoEstabelecimento,
          cidade: cidadeEstabelecimento,
          cep: cepEstabelecimento,
          uf: ufEstabelecimento,
          cargo: cargo,
          estado_civil: estadoCivil,
          rg: rg,
          cpf: cpf,
          data_nascimento: dataNascimento,
          nome_fantasia: nomeFantasia,
          razao_social: razaoSocial,
          cnpj: cnpj,
          cnae: cnae,
          sessao_cnae: sessaoCnae,
          tipo_de_empresa: tipoEmpresa,
          telefone_pessoal: telefonePessoal,
          descricao_telefone_pessoal: descricaotelefonePessoal,
          telefone: telefoneEstabelecimento,
          descricao: descricaotelefoneEstabelecimento,
          nome_banco: nomeBanco,
          agencia: agencia,
          numero_conta: numeroConta,
          nome_titular: nomeTitular,
          tipo_conta: tipoConta,
          conta_juridica: 0, //Informa o tipo de conta
          cpf_cnpj: cpfCnpj,
          numero_banco: numeroBanco,
          serial_number: serialNumber,
          licenciado_id: licenciado.data.data[0].id //Informa o id do lincenciado logado
        });
        // STOO LOADING
        setLoading(false)
        window.location.reload();
      } catch (err) {
          // STOO LOADING
          setLoading(false)
          console.log(err);
          setError(
            "Houve um problema ao realizar o cadastro."
          );
      }
    }
  }

  return (
    <Container fluid="md">
      <NavBar />

      <h2>Cadastro de Estabelecimento</h2>

      {error && <Alert variant="danger">{error}</Alert>}
      <Row>
        <Col>
            <Form.Group className="mb-3">
              <Form.Label>Nome*</Form.Label>
              <Form.Control 
                type="nome"
                placeholder="Nome"
                value={nome}
                onChange={(e) => setNome(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>E-mail*</Form.Label>
              <Form.Control 
                type="email"
                placeholder="E-mail"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Rua Pessoal*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Rua Pessoal"
                value={ruaPessoal}
                onChange={(e) => setRuaPessoal(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Número Pessoal*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Número Pessoal"
                value={numeroPessoal}
                onChange={(e) => setNumeroPessoal(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Bairro Pessoal*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Bairro Pessoal"
                value={bairroPessoal}
                onChange={(e) => setBairroPessoal(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Complemento Pessoal*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Complemento Pessoal"
                value={complementoPessoal}
                onChange={(e) => setComplementoPessoal(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Cidade Pessoal*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Cidade Pessoal"
                value={cidadePessoal}
                onChange={(e) => setCidadePessoal(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>CEP Pessoal*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="CEP Pessoal"
                value={cepPessoal}
                onChange={(e) => setCepPessoal(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>UF Pessoal*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="UF Pessoal"
                value={ufPessoal}
                onChange={(e) => setUfPessoal(e.target.value)}
                maxLength="2"
                style={{textTransform: "uppercase"}}
              />
            </Form.Group>

            {/* CONFIGURA LOADING AO CLICAR EM CADASTRAR */}
            {
              loading ? 
              (
                <Spinner variant="primary" animation="border" role="status" />
              ) : (
                <Button onClick={handleEstabelecimento} variant="primary">
                  Cadastrar
                </Button>
              )
            }
        </Col>

        <Col>
          <Form.Group className="mb-3">
              <Form.Label>Rua Estabelecimento*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Rua Estabelecimento"
                value={ruaEstabelecimeto}
                onChange={(e) => setRuaEstabelecimeto(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Número Estabelecimento*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Número Estabelecimento"
                value={numeroEstabelecimento}
                onChange={(e) => setNumeroEstabelecimento(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Bairro Estabelecimento*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Bairro Estabelecimento"
                value={bairroEstabelecimento}
                onChange={(e) => setBairroEstabelecimento(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Complemento Estabelecimento*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Complemento Estabelecimento"
                value={complementoEstabelecimento}
                onChange={(e) => setComplementoEstabelecimento(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Cidade Estabelecimento*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Cidade Estabelecimento"
                value={cidadeEstabelecimento}
                onChange={(e) => setCidadeEstabelecimento(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>CEP Estabelecimento*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="CEP Estabelecimento"
                value={cepEstabelecimento}
                onChange={(e) => setCepEstabelecimento(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>UF Estabelecimento*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="UF Estabelecimento"
                value={ufEstabelecimento}
                onChange={(e) => setUfEstabelecimento(e.target.value)}
                maxLength="2"
                style={{textTransform: "uppercase"}}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Cargo*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Cargo"
                value={cargo}
                onChange={(e) => setCargo(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Estado Civil*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Estado Civil"
                value={estadoCivil}
                onChange={(e) => setEstadoCivil(e.target.value)}
              />
            </Form.Group>
        </Col>

        <Col>
            <Form.Group className="mb-3">
              <Form.Label>RG*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="RG"
                value={rg}
                onChange={(e) => setRg(e.target.value)}
              />
            </Form.Group>


            <Form.Group className="mb-3">
              <Form.Label>CPF*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="CPF"
                value={cpf}
                onChange={(e) => setCpf(e.target.value)}
              />
            </Form.Group>


            <Form.Group className="mb-3">
              <Form.Label>Data Nascimento*</Form.Label>
              <Form.Control 
                type="date"
                placeholder="Data Nascimento"
                value={dataNascimento}
                onChange={(e) => setDataNascimento(e.target.value)}
              />
            </Form.Group>

            
            <Form.Group className="mb-3">
              <Form.Label>Nome Fantasia*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Nome Fantasia"
                value={nomeFantasia}
                onChange={(e) => setNomeFantasia(e.target.value)}
              />
            </Form.Group>

                        
            <Form.Group className="mb-3">
              <Form.Label>Razao Social*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Razao Social"
                value={razaoSocial}
                onChange={(e) => setRazaoSocial(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>CNPJ*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="CNPJ"
                value={cnpj}
                onChange={(e) => setCnpj(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>CNAE*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="CNAE"
                value={cnae}
                onChange={(e) => setCnae(e.target.value)}
              />
            </Form.Group>
        </Col>

        <Col>
           <Form.Group className="mb-3">
              <Form.Label>Sessão CNAE*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Sessão CNAE"
                value={sessaoCnae}
                onChange={(e) => setsessaoCnae(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Tipo Empresa*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Tipo Empresa"
                value={tipoEmpresa}
                onChange={(e) => setTipoEmpresa(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Telefone Pessoal*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Telefone Pessoal"
                value={telefonePessoal}
                onChange={(e) => setTelefonePessoal(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Descrição Telefone Pessoal*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Descrição Telefone Pessoal"
                value={descricaotelefonePessoal}
                onChange={(e) => setDescricaoTelefonePessoal(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Telefone Estabelecimento*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Telefone Estabelecimento"
                value={telefoneEstabelecimento}
                onChange={(e) => setTelefoneEstabelecimento(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Descrição Telefone Estabelecimento*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Descrição Telefone Estabelecimento"
                value={descricaotelefoneEstabelecimento}
                onChange={(e) => setDescricaoTelefoneEstabelecimento(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Serial Number*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Serial Number"
                value={serialNumber}
                onChange={(e) => setSerialNumber(e.target.value)}
              />
            </Form.Group>
        </Col>

        <Col>
           <Form.Group className="mb-3">
              <Form.Label>Nome do Banco*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Nome do Banco"
                value={nomeBanco}
                onChange={(e) => setNomeBanco(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Agência*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Agência"
                value={agencia}
                onChange={(e) => setAgencia(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Número da Conta*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Número da Conta"
                value={numeroConta}
                onChange={(e) => setNumeroConta(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Nome Titular*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Nome Titular"
                value={nomeTitular}
                onChange={(e) => setNomeTitular(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
                <Form.Label>Tipo de Conta*</Form.Label>
                <Form.Select value={tipoConta} onChange={(e) => setTipoConta(e.target.value)}>
                    <option>Selecione</option>
                    <option value="corrente">Corrente</option>
                    <option value="poupanca">Poupança</option>
                </Form.Select>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>CPF ou CNPJ*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="CPF ou CNPJ"
                value={cpfCnpj}
                onChange={(e) => setCpfCnpj(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Código do Banco*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Código do Banco"
                value={numeroBanco}
                onChange={(e) => setNumeroBanco(e.target.value)}
              />
            </Form.Group>
        </Col>
      </Row>
    </Container>
  );
};

export default EstabelecimentoAdd;