import React, { useCallback, useState } from "react";
import { Alert, Button, Col, Container, Form, Row, Spinner } from "react-bootstrap";
import Dropzone from 'react-dropzone';

import { uniqueId } from "lodash";
import filesize from "filesize";

import NavBar from "../../components/NavBar";
import api from "../../services/api";

const TransacaoContaDeLuzAdd = () => {
    const [error, setError] = useState("");

    const [nome, setNome] = useState("");
    const [email, setEmail] = useState("");
    const [telefone, setTelefone] = useState("");
    const [telefone2, setTelefone2] = useState("");
    const [telefone3, setTelefone3] = useState("");
    const [profissao, setProfissao] = useState("");

    const [rg, setRg] = useState("");
    const [rgEmissor, setRgEmissor] = useState("");
    const [rgUf, setRgUf] = useState("");
    const [rgDataEmissao, setRgDataEmissao] = useState("");
    const [sexo, setSexo] = useState("");
    const [cpf, setCpf] = useState("");
    const [dataNascimento, setDataNascimento] = useState("");

    const [nomeBanco, setNomeBanco] = useState("");
    const [agencia, setAgencia] = useState("");
    const [numeroConta, setNumeroConta] = useState("");
    const [nomeTitular, setNomeTitular] = useState("");
    const [tipoConta, setTipoConta] = useState("");

    const [numeroParcela, setNumeroParcela] = useState("");

    const [valorPagamento, setValorPagamento] = useState("");


    const [uploadedDocumentoContaLuz, setUploadedDocumentoContaLuz] = useState([]);
    const [previewDocumentoContaLuz, setPreviewDocumentoContaLuz] = useState("");

    const [uploadedDocumentoComFoto, setUploadedDocumentoComFoto] = useState([]);
    const [previewDocumentoComFoto, setPreviewDocumentoComFoto] = useState("");

    const [loading, setLoading] = useState(false);

    const [nacionalidade, setNacionalidade] = useState("");
    const [naturalidade, setNaturalidade] = useState("");
    const [estadoCivil, setEstadoCivil] = useState("");
    const [pai, setPai] = useState("");
    const [mae, setMae] = useState("");
    const [rendaFamiliar, setRendaFamiliar] = useState("");

    const [contatoAdicional1, setContatoAdicional1] = useState("");
    const [enderecoAdicional1, setEnderecoAdicional1] = useState("");
    const [telefoneAdicional1, setTelefoneAdicional1] = useState("");

    const [contatoAdicional2, setContatoAdicional2] = useState("");
    const [enderecoAdicional2, setEnderecoAdicional2] = useState("");
    const [telefoneAdicional2, setTelefoneAdicional2] = useState("");

    const [rua, setRua] = useState("");
    const [numero, setNumero] = useState("");
    const [bairro, setBairro] = useState("");
    const [cidade, setCidade] = useState("");
    const [cep, setCep] = useState("");
    const [uf, setUf] = useState("");

    const [componhia, setComponhia] = useState("");
    const [dataLeitura, setDataLeitura] = useState("");
    const [numeroInstalacao, setNumeroInstalacao] = useState("");

    // R$ 1000 - 15, 16 e 18
    // R$ 900 - 12, 15, 16 e 18
    // R$ 800 - 12, 15, 16 e 18
    // R$ 700 - 12, 15, 16 e 18
    // R$ 600 - 16 e 18
    // R$ 500 - 12, 15, 16 e 18
    // R$ 450 - 8, 12, 15, 16 e 18
    // R$ 400 - 12, 15, 16 e 18
    // R$ 350 - 8, 12, 15 e 16
    // R$ 300 - 8, 12, 15 e 16
    // R$ 250 - 8, 12, 15 e 16
    // R$ 200 - 8, 12, 15 e 16

    async function handleTransaction() {
      if (!nome) {
        setError("Preencha os campos obrigatórios para continuar.");
      } else {
        try {
          // START LOADING
          setLoading(true)
          const data = new FormData();

          uploadedDocumentoContaLuz.forEach(item => {
            data.append(`image${uploadedDocumentoContaLuz.indexOf(item)}`, item.file);
          });

          // Envia PARA API
          const documentoContaLuz = await api.post("/documento_upload_mult", data, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          });

          uploadedDocumentoComFoto.forEach(item => {
            data.append(`image${uploadedDocumentoComFoto.indexOf(item)}`, item.file);
          });

          // Envia PARA API
          const documentoComFoto = await api.post("/documento_upload_mult", data, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          });

          // FAZ A REQUISIÇÃO PARA CADASTRAR UMA TRANSAÇÃO
          const cliente = await api.post("/energy/transaction/client", 
          { 
              email: email, 
              name: nome,
              cpf: cpf,
              profissao: profissao,
              data_aniversario: dataNascimento,
              rg: rg,
              rg_emissor: rgEmissor,
              rg_uf: rgUf,
              rg_data_emissao: rgDataEmissao,
              sex: sexo,
              nacionalidade: nacionalidade,
              naturalidade: naturalidade,
              estado_civil: estadoCivil,
              numero_telefone_1: telefone,
              numero_telefone_2: telefone2,
              numero_telefone_3: telefone3,
              nome_pai: pai,
              nome_mae: mae,
              renda_familiar: rendaFamiliar,
              contato_adicional_1_nome: contatoAdicional1,
              contato_adicional_1_endereco: enderecoAdicional1,
              contato_adicional_1_numero_telefone: telefoneAdicional1,
              contato_adicional_2_nome: contatoAdicional2,
              contato_adicional_2_endereco: enderecoAdicional2,
              contato_adicional_2_numero_telefone: telefoneAdicional2,
              banco_agencia: agencia,
              banco_conta: numeroConta,
              banco_tipo_conta: tipoConta,
              banco_nome: nomeBanco,
              endereco_rua: rua,
              endereco_numero: numero,
              endereco_bairro: bairro,
              endereco_cidade: cidade,
              endereco_cep: cep,
              endereco_uf: uf 
          });

          // FAZ A REQUISIÇÃO PARA SABER O VALOR DAS TAXAS
          const simulador = await api.post("/energy/transaction/simulation", {
              parcelas: numeroParcela,
              valor: valorPagamento
          });

          await api.post("/energy/transaction", {
              numero_parcelas: simulador.data.data.parcelas,
              valor_pagamento: simulador.data.data.valor_pagamento,
              valor_adicional: simulador.data.data.valor_adicional,
              valor_final: simulador.data.data.valor_total,
              taxa: simulador.data.data.taxa,
              valor_parcela: simulador.data.data.valor_parcela,
              companhia: componhia,
              cliente_id: cliente.data.data.id,
              documento_conta_de_luz_id: documentoContaLuz.data.data.id,
              documento_pessoal_com_foto_id: documentoComFoto.data.data.id,
              data_leitura: dataLeitura,
              numero_instalacao: numeroInstalacao
          });

        // STOP LOADING
        setLoading(false)

        window.location.reload();
        } catch (err) {
            // STOP LOADING
            setLoading(false)
            console.log(err);
            setError(
              "Houve um problema ao realizar o cadastro."
            );
        }
      }
    }

    // PREPARA DOCUMENTO CONTA DE LUZ PARA UPLOAD
    const submitFileDocumentoContaLuz = useCallback((file) => {
        const uploadFile = {
          file: file[0],
          id: uniqueId(),
          name: file[0].name,
          readableSize: filesize(file[0].size),
          preview: URL.createObjectURL(file[0]),
        };
  
        setUploadedDocumentoContaLuz([...uploadedDocumentoContaLuz, uploadFile]);
        setPreviewDocumentoContaLuz(uploadFile.preview);
    }, [uploadedDocumentoContaLuz]);

    // PREPARA TERMO PARA UPLOAD
    const submitFileDocumentoComFoto = useCallback((file) => {
        const uploadFile = {
          file: file[0],
          id: uniqueId(),
          name: file[0].name,
          readableSize: filesize(file[0].size),
          preview: URL.createObjectURL(file[0]),
        };
  
        setUploadedDocumentoComFoto([...uploadedDocumentoComFoto, uploadFile]);
        setPreviewDocumentoComFoto(uploadFile.preview);
    }, [uploadedDocumentoComFoto]);

    return (
    <Container fluid="md">
      <NavBar />

      <h2>Cadastro de Transação Conta de Luz</h2>

      {error && <Alert variant="danger">{error}</Alert>}
      <Row>
        <Col>
            <Form.Group className="mb-3">
              <Form.Label>Nome*</Form.Label>
              <Form.Control 
                type="nome"
                placeholder="Nome"
                value={nome}
                onChange={(e) => setNome(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>E-mail*</Form.Label>
              <Form.Control 
                type="email"
                placeholder="E-mail"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>RG*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="RG"
                value={rg}
                onChange={(e) => setRg(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>RG Emissor*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="RG Emissor"
                value={rgEmissor}
                onChange={(e) => setRgEmissor(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>RG UF*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="RG UF"
                value={rgUf}
                onChange={(e) => setRgUf(e.target.value)}
                maxLength="2"
                style={{textTransform: "uppercase"}}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Data de Emissão*</Form.Label>
              <Form.Control 
                type="date"
                placeholder="Data de Emissão"
                value={rgDataEmissao}
                onChange={(e) => setRgDataEmissao(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>CPF*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="CPF"
                value={cpf}
                onChange={(e) => setCpf(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Telefone*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Telefone"
                value={telefone}
                onChange={(e) => setTelefone(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
                  <Form.Label>Profissão*</Form.Label>
                  <Form.Control 
                    type="text"
                    placeholder="Profissão"
                    value={profissao}
                    onChange={(e) => setProfissao(e.target.value)}
                  />
            </Form.Group>

            <Form.Group  className="mb-3">
                  <Form.Label>Sexo*</Form.Label>
                  <Form.Select value={sexo} onChange={(e) => setSexo(e.target.value)}>
                      <option>Selecione</option>
                      <option value="M" >Masculino</option>
                      <option value="F">Feminino</option>
                      <option value="O">Outros</option>
                  </Form.Select>
            </Form.Group>

            <Form.Group className="mb-3">
                  <Form.Label>Data de Nascimento*</Form.Label>
                  <Form.Control 
                    type="date"
                    placeholder="Data de Nascimento"
                    value={dataNascimento}
                    onChange={(e) => setDataNascimento(e.target.value)}
                  />
            </Form.Group>

            <Form.Group className="mb-3">
                  <Form.Label>Nacionalidade*</Form.Label>
                  <Form.Control 
                    type="text"
                    placeholder="Nacionalidade"
                    value={nacionalidade}
                    onChange={(e) => setNacionalidade(e.target.value)}
                  />
            </Form.Group>

            <Form.Group className="mb-3">
                  <Form.Label>Naturalidadde*</Form.Label>
                  <Form.Control 
                    type="text"
                    placeholder="Naturalidadde"
                    value={naturalidade}
                    onChange={(e) => setNaturalidade(e.target.value)}
                  />
            </Form.Group>

            <Form.Group className="mb-3">
                  <Form.Label>Estado Civil*</Form.Label>
                  <Form.Control 
                    type="text"
                    placeholder="Estado Civil"
                    value={estadoCivil}
                    onChange={(e) => setEstadoCivil(e.target.value)}
                  />
            </Form.Group>

            {/* CONFIGURA LOADING AO CLICAR EM CADASTRAR */}
            {
              loading ? 
              (
                <Spinner variant="primary" animation="border" role="status" />
              ) : (
                <Button onClick={handleTransaction} variant="primary">
                  Cadastrar
                </Button>
              )
            }
        </Col>

        <Col>
            <Form.Group className="mb-3">
              <Form.Label>Telefone 2*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Telefone 2"
                value={telefone2}
                onChange={(e) => setTelefone2(e.target.value)}
              />
            </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Rua *</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Rua "
                value={rua}
                onChange={(e) => setRua(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Número *</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Número "
                value={numero}
                onChange={(e) => setNumero(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Bairro *</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Bairro "
                value={bairro}
                onChange={(e) => setBairro(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Cidade *</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Cidade "
                value={cidade}
                onChange={(e) => setCidade(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>CEP *</Form.Label>
              <Form.Control 
                type="text"
                placeholder="CEP "
                value={cep}
                onChange={(e) => setCep(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>UF *</Form.Label>
              <Form.Control 
                type="text"
                placeholder="UF "
                value={uf}
                onChange={(e) => setUf(e.target.value)}
                maxLength="2"
                style={{textTransform: "uppercase"}}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Telefone 3*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Telefone 3"
                value={telefone3}
                onChange={(e) => setTelefone3(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Nome do Pai*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Nome do Pai"
                value={pai}
                onChange={(e) => setPai(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Nome do Mae*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Nome do Mae"
                value={mae}
                onChange={(e) => setMae(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Renda Familiar*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Renda Familiar"
                value={rendaFamiliar}
                onChange={(e) => setRendaFamiliar(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Contato Adicional 1*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Contato Adicional 1"
                value={contatoAdicional1}
                onChange={(e) => setContatoAdicional1(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Endereço Adicional 1*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Endereço Adicional 1"
                value={enderecoAdicional1}
                onChange={(e) => setEnderecoAdicional1(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Telefone Adicional 1*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Telefone Adicional 1"
                value={telefoneAdicional1}
                onChange={(e) => setTelefoneAdicional1(e.target.value)}
              />
            </Form.Group>

        </Col>

        <Col>
        
            <Form.Group className="mb-3">
              <Form.Label>Contato Adicional 2*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Contato Adicional 2"
                value={contatoAdicional2}
                onChange={(e) => setContatoAdicional2(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Endereço Adicional 2*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Endereço Adicional 2"
                value={enderecoAdicional2}
                onChange={(e) => setEnderecoAdicional2(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Telefone Adicional 2*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Telefone Adicional 2"
                value={telefoneAdicional2}
                onChange={(e) => setTelefoneAdicional2(e.target.value)}
              />
            </Form.Group>

           <Form.Group  className="mb-3">
              <Form.Label>Valor*</Form.Label>
              <Form.Select value={valorPagamento} onChange={(e) => setValorPagamento(e.target.value)}>
                  <option>Selecione uma valor</option>
                  <option value="200" >R$ 200</option>
                  <option value="250">R$ 250</option>
                  <option value="300">R$ 300</option>
                  <option value="350">R$ 350</option>
                  <option value="400">R$ 400</option>
                  <option value="500">R$ 500</option>
                  <option value="600">R$ 600</option>
                  <option value="700">R$ 700</option>
                  <option value="800">R$ 800</option>
                  <option value="900">R$ 900</option>
                  <option value="1000">R$ 1000</option>
              </Form.Select>
            </Form.Group>

            {/* // R$ 1000 - 15, 16 e 18
            // R$ 900 - 12, 15, 16 e 18
            // R$ 800 - 12, 15, 16 e 18
            // R$ 700 - 12, 15, 16 e 18 
            // R$ 600 - 16 e 18 
            // R$ 500 - 12, 15, 16 e 18 
            // R$ 450 - 8, 12, 15, 16 e 18 
            // R$ 400 - 12, 15, 16 e 18 
            // R$ 350 - 8, 12, 15 e 16 
            // R$ 300 - 8, 12, 15 e 16 
            // R$ 250 - 8, 12, 15 e 16 
            // R$ 200 - 8, 12, 15 e 16  */}
           <Form.Group  className="mb-3">
              <Form.Label>Numero de Parcelas*</Form.Label>
              <Form.Select value={numeroParcela} onChange={(e) => setNumeroParcela(e.target.value)}>
                  <option>Selecione uma valor</option>
                  {
                    (valorPagamento === "200" || valorPagamento === "250" || valorPagamento === "300" || valorPagamento === "350") && (
                      <>
                        <option value="8" >8X</option>
                        <option value="12">12x</option>
                        <option value="15">15x</option>
                        <option value="16">16x</option>
                      </>
                    )
                  }

                  {
                    (valorPagamento === "400" || valorPagamento === "500" || valorPagamento === "700" || valorPagamento === "800" || valorPagamento === "900") && (
                      <>
                        <option value="12">12x</option>
                        <option value="15">15x</option>
                        <option value="16">16x</option>
                        <option value="18">18x</option>
                      </>
                    )
                  }

                  {
                    (valorPagamento === "450") && (
                      <>
                        <option value="8" >8X</option>
                        <option value="12">12x</option>
                        <option value="15">15x</option>
                        <option value="16">16x</option>
                        <option value="18">18x</option>
                      </>
                    )
                  }

                  {
                    (valorPagamento === "600") && (
                      <>
                        <option value="16">16x</option>
                        <option value="18">18x</option>
                      </>
                    )
                  }
              </Form.Select>
            </Form.Group>

            <Form.Group className="mb-3">
                <Form.Label>Componhia*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Componhia"
                  value={componhia}
                  onChange={(e) => setComponhia(e.target.value)}
                />
            </Form.Group>

            <Form.Group className="mb-3">
                <Form.Label>Data da Leitura*</Form.Label>
                <Form.Control 
                  type="date"
                  placeholder="Data da Leitura"
                  value={dataLeitura}
                  onChange={(e) => setDataLeitura(e.target.value)}
                />
            </Form.Group>

            <Form.Group className="mb-3">
                <Form.Label>Número de Instalação*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Número de Instalação"
                  value={numeroInstalacao}
                  onChange={(e) => setNumeroInstalacao(e.target.value)}
                />
            </Form.Group>

            {/* SELECIONA A ASSINATUA */}
            <Form.Group className="mb-3">
                <Form.Label>Upload documento conta de luz*</Form.Label>
                <br/>
                <Dropzone  accept="image/jpeg,image/jpg,image/png,.pdf" onDropAccepted={file => submitFileDocumentoContaLuz(file)}>
                    { ({ getRootProps, getInputProps }) => (
                      <>
                          <div
                            {...getRootProps()}
                            className="btn btn-secondary"
                          >
                            <input {...getInputProps()} />
                            Upload
                          </div>
                          <br/>
                          {/* Priview do documento */}
                          <img style={{width: 100, marginTop: 5}}  src={previewDocumentoContaLuz} alt=""/>
                      </>
                    ) }
                </Dropzone>
            </Form.Group>

            {/* SELECIONA O DOCUMENTO COM FOTO */}
            <Form.Group className="mb-3">
                <Form.Label>Upload do documento com foto*</Form.Label>
                <br/>
                <Dropzone  accept="image/jpeg,image/jpg,image/png,.pdf" onDropAccepted={file => submitFileDocumentoComFoto(file)}>
                    { ({ getRootProps, getInputProps }) => (
                      <>
                          <div
                            {...getRootProps()}
                            className="btn btn-secondary"
                          >
                            <input {...getInputProps()} />
                            Upload
                          </div>
                          <br/>
                          {/* Priview do documento */}
                          <img style={{width: 100, marginTop: 5}}  src={previewDocumentoComFoto} alt=""/>
                      </>
                    ) }
                </Dropzone>
            </Form.Group>
        </Col>

        <Col>
              <Form.Group className="mb-3">
                <Form.Label>Nome do Banco*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Nome do Banco"
                  value={nomeBanco}
                  onChange={(e) => setNomeBanco(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Agência*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Agência"
                  value={agencia}
                  onChange={(e) => setAgencia(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Número da Conta*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Número da Conta"
                  value={numeroConta}
                  onChange={(e) => setNumeroConta(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Nome Titular*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Nome Titular"
                  value={nomeTitular}
                  onChange={(e) => setNomeTitular(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
              <Form.Label>Tipo de Conta*</Form.Label>
                <Form.Select value={tipoConta} onChange={(e) => setTipoConta(e.target.value)}>
                    <option>Selecione</option>
                    <option value="corrente">Corrente</option>
                    <option value="poupanca">Poupança</option>
                </Form.Select>
              </Form.Group>
        </Col>
      </Row>
    </Container>
  );
};

export default TransacaoContaDeLuzAdd;