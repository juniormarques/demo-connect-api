import React, { useCallback, useState } from "react";
import { Alert, Button, Col, Container, Form, Row, Spinner } from "react-bootstrap";
import Dropzone from 'react-dropzone';

import { uniqueId } from "lodash";
import filesize from "filesize";

import NavBar from "../../components/NavBar";
import api from "../../services/api";

const TransacaoAdd = () => {
    const [error, setError] = useState("");

    const [nome, setNome] = useState("");
    const [email, setEmail] = useState("");
    const [telefone, setTelefone] = useState("");

    const [rg, setRg] = useState("");
    const [cpf, setCpf] = useState("");

    const [nomeBanco, setNomeBanco] = useState("");
    const [agencia, setAgencia] = useState("");
    const [numeroConta, setNumeroConta] = useState("");
    const [nomeTitular, setNomeTitular] = useState("");
    const [tipoConta, setTipoConta] = useState("");
    const [cpfCnpj, setCpfCnpj] = useState("");
    const [contaJuridica, setContaJuridica] = useState("");

    const [numeroParcela, setNumeroParcela] = useState("");
    const [bandeira, setBandeira] = useState("");
    const [quantidadeBoleto, setQuantidadeBoleto] = useState("");

    const [taxaServico, setTaxaServico] = useState("");
    const [valorAcrescimo, setValorAcrescimo] = useState("");
    const [valorPagamento, setValorPagamento] = useState("");

    const [uploadedTermo, setUploadedTermo] = useState([]);
    const [previewTermo, setPreviewTermo] = useState("");

    const [uploadedAssinatura, setUploadedAssinatura] = useState([]);
    const [previewAssinatura, setPreviewAssinatura] = useState("");

    const [uploadedDocumentoComFoto, setUploadedDocumentoComFoto] = useState([]);
    const [previewDocumentoComFoto, setPreviewDocumentoComFoto] = useState("");

    const [uploadedBoleto, setUploadedBoleto] = useState([]);
    const [previewBoleto, setPreviewBoleto] = useState("");

    const [codigoDeBarras, setCodigoDeBarras] = useState("");
    const [valorBoleto, setValorBoleto] = useState("");
    const [dataVencimentoBoleto, setDataVencimentoBoleto] = useState("");

    const [loading, setLoading] = useState(false);
    
    // ROTA DE SIMULAÇÃO. OBTÉM O CALCULO DAS TAXAS
    const url = `/get_sim?numero_parcelas=${numeroParcela}&bandeira=${bandeira}&maquineta=${"Celer"}&conjunto_de_taxas=${"PDV4"}&valor_acrescimo=${valorAcrescimo}&valor_pagamento=${valorPagamento}&taxa_servico=${taxaServico}`;
    // console.log(bandeira);

    async function handleTransaction() {
      if (!nome) {
        setError("Preencha os campos obrigatórios para continuar.");
      } else {
        try {
          // START LOADING
          setLoading(true)
          const data = new FormData();

          uploadedTermo.forEach(item => {
            data.append(`image${uploadedTermo.indexOf(item)}`, item.file);
          });

          // Envia PARA API
          const termo = await api.post("/documento_upload_mult", data, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          });

          uploadedAssinatura.forEach(item => {
            data.append(`image${uploadedAssinatura.indexOf(item)}`, item.file);
          });

          // Envia PARA API
          const assinatura = await api.post("/documento_upload_mult", data, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          });

          uploadedDocumentoComFoto.forEach(item => {
            data.append(`image${uploadedDocumentoComFoto.indexOf(item)}`, item.file);
          });

          // Envia PARA API
          const documentoComFoto = await api.post("/documento_upload_mult", data, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          });

          // FAZ A REQUISIÇÃO PARA SABER O VALOR DAS TAXAS PARA REALIZAR O CADASTRO DA TRANSAÇÃO
          const simulador = await api.get(url);

          // FAZ A REQUISIÇÃO PARA CADASTRAR UMA TRANSAÇÃO
          const transacao = await api.post("/transacao", 
          { 
            email: email, 
            nome: nome,
            cpf: cpf,
            rg: rg,
            telefone: telefone,
            numero_parcelas: numeroParcela,
            bandeira: bandeira,
            quantidade_boleto: quantidadeBoleto,
            taxa_servico: simulador.data.data.taxa_servico,
            valor_acrescimo: valorAcrescimo,
            valor_pagamento: valorPagamento,
            valor_total: simulador.data.data.valor_total,
            conjunto_de_taxa: simulador.data.data.conjunto_de_taxa,
            maquineta: simulador.data.data.maquineta,
            valor_parcela: simulador.data.data.valor_parcela,
            numero_autorizacao: "-",
            id_doc_autorizacao: "-",
            id_doc_termo: termo.data.data.id,
            assinatura_id: assinatura.data.data.id,
            id_doc_documento_foto: documentoComFoto.data.data.id,
          });

          if(quantidadeBoleto === "0") {
              // CADASTRA DADOS BANCARIOS PARA DEPOSITO
              await api.post("/dados_banc_trans", 
              { 
                id_transacao: transacao.data.data.id,  
                nome_banco: nomeBanco,
                agencia: agencia,
                numero_conta: numeroConta,
                nome_titular: nomeTitular,
                tipo_conta: tipoConta,
                conta_juridica: contaJuridica, //Informa o tipo de conta
                CPF_CNPJ: cpfCnpj,
              });
          } 

          uploadedBoleto.forEach(item => {
            data.append(`image${uploadedBoleto.indexOf(item)}`, item.file);
          });

          // Envia PARA API
          const boletoUpload = await api.post("/documento_upload_mult", data, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          });

          if(quantidadeBoleto === "1") {
            // CADASTRA DADOS BANCARIOS PARA DEPOSITO
            await api.post("/boleto_trans", 
            { 
              id_transacao :"2db7467d-ce33-473c-bc8d-0a271d84fbb2",
              codigo_de_barras: codigoDeBarras,
              id_boleto: boletoUpload.data.data.id,
              valor: valorBoleto,
              data_vencimento: dataVencimentoBoleto
            });
        } 
          window.location.reload();
        } catch (err) {
            // STOP LOADING
            setLoading(false)
            console.log(err);
            setError(
              "Houve um problema ao realizar o cadastro."
            );
        }
      }
    }

    // PREPARA TERMO PARA UPLOAD
    const submitFileTermo = useCallback((file) => {
        const uploadFile = {
          file: file[0],
          id: uniqueId(),
          name: file[0].name,
          readableSize: filesize(file[0].size),
          preview: URL.createObjectURL(file[0]),
        };
  
        setUploadedTermo([...uploadedTermo, uploadFile]);
        setPreviewTermo(uploadFile.preview);
    }, [uploadedTermo]);

    // PREPARA TERMO PARA UPLOAD
    const submitFileAssinatura = useCallback((file) => {
        const uploadFile = {
          file: file[0],
          id: uniqueId(),
          name: file[0].name,
          readableSize: filesize(file[0].size),
          preview: URL.createObjectURL(file[0]),
        };
  
        setUploadedAssinatura([...uploadedAssinatura, uploadFile]);
        setPreviewAssinatura(uploadFile.preview);
    }, [uploadedAssinatura]);

    // PREPARA TERMO PARA UPLOAD
    const submitFileDocumentoComFoto = useCallback((file) => {
        const uploadFile = {
          file: file[0],
          id: uniqueId(),
          name: file[0].name,
          readableSize: filesize(file[0].size),
          preview: URL.createObjectURL(file[0]),
        };
  
        setUploadedDocumentoComFoto([...uploadedDocumentoComFoto, uploadFile]);
        setPreviewDocumentoComFoto(uploadFile.preview);
    }, [uploadedDocumentoComFoto]);

    // PREPARA TERMO PARA UPLOAD
    const submitFileBoleto = useCallback((file) => {
          const uploadFile = {
            file: file[0],
            id: uniqueId(),
            name: file[0].name,
            readableSize: filesize(file[0].size),
            preview: URL.createObjectURL(file[0]),
          };
    
          setUploadedBoleto([...uploadedBoleto, uploadFile]);
          setPreviewBoleto(uploadFile.preview);
      }, [uploadedBoleto]);

    return (
    <Container fluid="md">
      <NavBar />

      <h2>Cadastro de Transação</h2>

      {error && <Alert variant="danger">{error}</Alert>}
      <Row>
        <Col>
            <Form.Group  className="mb-3">
              <Form.Label>Numero de Parcelas*</Form.Label>
              <Form.Select value={numeroParcela} onChange={(e) => setNumeroParcela(e.target.value)}>
                  <option>Selecione uma valor</option>
                  <option value="1" >1X</option>
                  <option value="2">2x</option>
                  <option value="3">3x</option>
                  <option value="4">4x</option>
                  <option value="5">5x</option>
                  <option value="6">6x</option>
                  <option value="7">7x</option>
                  <option value="8">8x</option>
                  <option value="9">9x</option>
                  <option value="10">10x</option>
                  <option value="11">11x</option>
                  <option value="12">12x</option>
              </Form.Select>
            </Form.Group>

            <Form.Group  className="mb-3">
              <Form.Label>Bandeira*</Form.Label>
              <Form.Select value={bandeira} onChange={(e) => setBandeira(e.target.value)}>
                  <option>Selecione uma bandeira</option>
                  <option value="VME" >VME</option>
                  <option value="DA">DA</option>
              </Form.Select>
            </Form.Group>

            <Form.Group  className="mb-3">
              <Form.Label>Tipo de Pagamento*</Form.Label>
              <Form.Select value={quantidadeBoleto} onChange={(e) => setQuantidadeBoleto(e.target.value)}>
                  <option>Selecione o tipo</option>
                  <option value="1" >Boleto</option>
                  <option value="0">Deposito</option>
              </Form.Select>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Nome*</Form.Label>
              <Form.Control 
                type="nome"
                placeholder="Nome"
                value={nome}
                onChange={(e) => setNome(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>E-mail*</Form.Label>
              <Form.Control 
                type="email"
                placeholder="E-mail"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>RG*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="RG"
                value={rg}
                onChange={(e) => setRg(e.target.value)}
              />
            </Form.Group>


            <Form.Group className="mb-3">
              <Form.Label>CPF*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="CPF"
                value={cpf}
                onChange={(e) => setCpf(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Telefone*</Form.Label>
              <Form.Control 
                type="text"
                placeholder="Telefone"
                value={telefone}
                onChange={(e) => setTelefone(e.target.value)}
              />
            </Form.Group>

            {/* CONFIGURA LOADING AO CLICAR EM CADASTRAR */}
            {
              loading ? 
              (
                <Spinner variant="primary" animation="border" role="status" />
              ) : (
                <Button onClick={handleTransaction} variant="primary">
                  Cadastrar
                </Button>
              )
            }
        </Col>

        <Col>
            <Form.Group className="mb-3">
                <Form.Label>Taxa de Serviço*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="0.0"
                  value={taxaServico}
                  onChange={(e) => setTaxaServico(e.target.value)}
                />
            </Form.Group>
           
            <Form.Group className="mb-3">
                <Form.Label>Valor do Acrescimo*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="0.0"
                  value={valorAcrescimo}
                  onChange={(e) => setValorAcrescimo(e.target.value)}
                />
            </Form.Group>

            <Form.Group className="mb-3">
                <Form.Label>Valor do Pagamento*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="100"
                  value={valorPagamento}
                  onChange={(e) => setValorPagamento(e.target.value)}
                />
            </Form.Group>
            
            {/* SELECIONA O TERMO */}
            <Form.Group className="mb-3">
                <Form.Label>Upload de termo*</Form.Label>
                <br/>
                <Dropzone  accept="image/jpeg,image/jpg,image/png,.pdf" onDropAccepted={file => submitFileTermo(file)}>
                    { ({ getRootProps, getInputProps }) => (
                      <>
                          <div
                            {...getRootProps()}
                            className="btn btn-secondary"
                          >
                            <input {...getInputProps()} />
                            Upload
                          </div>
                          <br/>
                          {/* Priview do documento */}
                          <img style={{width: 100, marginTop: 5}}  src={previewTermo} alt=""/>
                      </>
                    ) }
                </Dropzone>
            </Form.Group>

            {/* SELECIONA A ASSINATUA */}
            <Form.Group className="mb-3">
                <Form.Label>Upload da assinatura*</Form.Label>
                <br/>
                <Dropzone  accept="image/jpeg,image/jpg,image/png,.pdf" onDropAccepted={file => submitFileAssinatura(file)}>
                    { ({ getRootProps, getInputProps }) => (
                      <>
                          <div
                            {...getRootProps()}
                            className="btn btn-secondary"
                          >
                            <input {...getInputProps()} />
                            Upload
                          </div>
                          <br/>
                          {/* Priview do documento */}
                          <img style={{width: 100, marginTop: 5}}  src={previewAssinatura} alt=""/>
                      </>
                    ) }
                </Dropzone>
            </Form.Group>

            {/* SELECIONA O DOCUMENTO COM FOTO */}
            <Form.Group className="mb-3">
                <Form.Label>Upload da documento com foto*</Form.Label>
                <br/>
                <Dropzone  accept="image/jpeg,image/jpg,image/png,.pdf" onDropAccepted={file => submitFileDocumentoComFoto(file)}>
                    { ({ getRootProps, getInputProps }) => (
                      <>
                          <div
                            {...getRootProps()}
                            className="btn btn-secondary"
                          >
                            <input {...getInputProps()} />
                            Upload
                          </div>
                          <br/>
                          {/* Priview do documento */}
                          <img style={{width: 100, marginTop: 5}}  src={previewDocumentoComFoto} alt=""/>
                      </>
                    ) }
                </Dropzone>
            </Form.Group>
        </Col>

        <Col>
          {/* SE NAO EXISTIR BOLETO MOSTRA OS DADOS PARA DEPOSITO */}
          {quantidadeBoleto === "0" && (
            <>
              <Form.Group className="mb-3">
                <Form.Label>Nome do Banco*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Nome do Banco"
                  value={nomeBanco}
                  onChange={(e) => setNomeBanco(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Agência*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Agência"
                  value={agencia}
                  onChange={(e) => setAgencia(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Número da Conta*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Número da Conta"
                  value={numeroConta}
                  onChange={(e) => setNumeroConta(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Nome Titular*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Nome Titular"
                  value={nomeTitular}
                  onChange={(e) => setNomeTitular(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
              <Form.Label>Tipo de Conta*</Form.Label>
                <Form.Select value={tipoConta} onChange={(e) => setTipoConta(e.target.value)}>
                    <option>Selecione</option>
                    <option value="corrente">Corrente</option>
                    <option value="poupanca">Poupança</option>
                </Form.Select>
              </Form.Group>

              <Form.Group  className="mb-3">
                <Form.Label>Conta*</Form.Label>
                <Form.Select value={contaJuridica} onChange={(e) => setContaJuridica(e.target.value)}>
                    <option>Selecione</option>
                    <option value="0">Pessoa Física</option>
                    <option value="1">Pessoa Jurídica</option>
                </Form.Select>
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>CPF ou CNPJ*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="CPF ou CNPJ"
                  value={cpfCnpj}
                  onChange={(e) => setCpfCnpj(e.target.value)}
                />
              </Form.Group>
            </>
          )}  
           
          {/* SE EXISTIR BOLETO MOSTRA OS DADOS PARA CADASTRO DE BOLETO */}
          {quantidadeBoleto >= "1" && (
            <>
              <Form.Group className="mb-3">
                <Form.Label>Código de Barras*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Código de Barras"
                  value={codigoDeBarras}
                  onChange={(e) => setCodigoDeBarras(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Valor*</Form.Label>
                <Form.Control 
                  type="text"
                  placeholder="Valor"
                  value={valorBoleto}
                  onChange={(e) => setValorBoleto(e.target.value)}
                />
              </Form.Group>


              <Form.Group className="mb-3">
                <Form.Label>Data de Vencimento*</Form.Label>
                <Form.Control 
                  type="date"
                  placeholder="Data de Vencimento"
                  value={dataVencimentoBoleto}
                  onChange={(e) => setDataVencimentoBoleto(e.target.value)}
                />
              </Form.Group>

                          
            {/* SELECIONA O BOLETO */}
            <Form.Group className="mb-3">
                <Form.Label>Upload de Boleto*</Form.Label>
                <br/>
                <Dropzone  accept="image/jpeg,image/jpg,image/png,.pdf" onDropAccepted={file => submitFileBoleto(file)}>
                    { ({ getRootProps, getInputProps }) => (
                      <>
                          <div
                            {...getRootProps()}
                            className="btn btn-secondary"
                          >
                            <input {...getInputProps()} />
                            Upload
                          </div>
                          <br/>
                          {/* Priview do documento */}
                          <img style={{width: 100, marginTop: 5}}  src={previewBoleto} alt=""/>
                      </>
                    ) }
                </Dropzone>
            </Form.Group>
            </>
          )}            
        </Col>
      </Row>
    </Container>
  );
};

export default TransacaoAdd;