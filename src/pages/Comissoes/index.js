import { format } from "date-fns";
import React, { useEffect, useState} from "react";
import {Container, Row, Table } from "react-bootstrap";

import NavBar from "../../components/NavBar";
import api from "../../services/api";

const Comissoes = () => {
    const [comissoes, setComissoes] = useState([]);

    useEffect(() => {
      async function loadComissoes() {
        const response = await api.get("/comissao");
        setComissoes(response.data.data);
      }

      loadComissoes();
    },[]);

    console.log();

    return (
    <Container fluid="md">
      <NavBar />

      <h2>Lista comissões</h2>

      <Row>
        <Table striped bordered hover>
            <thead>
              <tr>
                <th>Valor</th>
                <th>Taxa</th>
                <th>Data</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {
                comissoes.map(comissao => (
                  <tr>
                    <td>{comissao.valor_comissao}</td>
                    <td>{comissao.taxa_comissao}</td>
                    <td>{format(new Date(comissao.created_at), "dd/MM/yyyy")}</td>
                    <td>{comissao.status}</td>
                  </tr>
                ))
              }
            </tbody>
          </Table>  
      </Row>
    </Container>
  );
};

export default Comissoes;