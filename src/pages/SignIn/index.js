import React, { useState } from "react";
import { Alert, Button, Card, Form, FormGroup } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

import api from "../../services/api";
import { login, user } from "../../services/auth";

import { Container } from "./styles";

const SignIn = () => {
  const location = useNavigate();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  async function handleSignIn() {
    if (!email || !password) {
      setError("Preencha e-mail e senha para continuar!");
    } else {
      try {
        const response = await api.post("/login", { email, password });
        login(response.data.token);
        user(JSON.stringify(response.data.user));
        console.log(response.data.token);
        location("/licenciadoadd");
      } catch (err) {
          console.log(err);
        setError(
          "Houve um problema com o login, verifique suas credenciais. T.T"
        );
      }
    }
  }

  return (
    <Container>
        <Card>
            <Card.Header>
                <h2>Diexa no Azul Teste</h2>
                {error && <Alert variant="danger">{error}</Alert>}
            </Card.Header>

            <FormGroup>
            <Form.Control 
                type="email"
                placeholder="E-mail"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
            />
            <Form.Control 
                type="password"
                placeholder="Senha"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
            />
            
            <Card.Footer>
                <Button onClick={handleSignIn}>Entrar</Button>
            </Card.Footer>
            </FormGroup>
        </Card>
    </Container>
  );
};

export default SignIn;