import axios from "axios";
import { getToken } from "./auth";

const api = axios.create({
  baseURL: "https://api.teste.deixanoazul.com.br/api"
});

api.interceptors.request.use(async config => {
  const token = getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

api.interceptors.response.use((response) => {
  if (response.data.message === "Token has expired" || response.data.message === "Token not provided") {
    // O token JWT expirou
    localStorage.removeItem("@appdeixanoazul-loggedUser");
    localStorage.removeItem("@appdeixanoazul-loggedUser");
  }

  return response;
});

export default api;