export const TOKEN_KEY = "@appdeixanoazul-Token";
const USER = "@appdeixanoazul-loggedUser";

export const isAuthenticated = () => localStorage.getItem(TOKEN_KEY) !== null;
export const getToken = () => localStorage.getItem(TOKEN_KEY);

export const login = token => {
  localStorage.setItem(TOKEN_KEY, token);
};

export const user = user => {
  localStorage.setItem(USER, user);
}

export const logout = () => {
  localStorage.removeItem(TOKEN_KEY);
  localStorage.removeItem(USER);
};
